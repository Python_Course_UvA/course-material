"""
This script shows basic data manipulation

- arithmetic on vectors

- basic statistics

- create random data

- sorting and searching

"""

import numpy as np # import the array calculation library

"""
Section 1:
basic manipulations of a vector

indexing a vector:

"""
data = np.arange(10) # create a vector from 0 to 9

print data[:3] # select the first three elements [0...2]

print data[5:] # select all elements from the fifth onwards

print data[:-1] # select all elements minus the last element of the vector

"""
vector arithmetic and basic statistics:
"""

data = data + 1 # add 1 to all elements in the vector [0,1...9] -> [1,2..10], try 'print data' before and after to see what happens.

average_one = np.mean(data) # calculate the average of the 'data' vector

"""
notice the difference between dividing by 3 versus dividing by 3. <- notice the '.' after 3?
"""
print 'data divided by 3\n', data/3
print 'data divided by 3.\n', data/3.

data = data/3. # divide the vector by three (the . is needed after '3' otherwise the result will be rounded of (int/float))

average_two = np.mean(data) # calculate the average of the 'data' vector after division


print 'average_one', average_one # print the first average
print 'average_two', average_two # print the second average



"""
Section 2:

randomly generated data

- creates a randomly generated array

- calculates averages over total, columns and rows of the array

- select elements of the array which are larger than a certain value

"""

Rarray = np.random.rand(5,5) # create a 5 by 5 random matrix (values between [0,1) )

print 'average of total random array', np.mean(Rarray) # calculate the average of the total array

print 'standard deviation of the array', np.std(Rarray) # calculate the standard deviation for the total array

print 'average over the columns of the array\n', np.mean(Rarray, axis = 0) # calculate the averages for all columns

print 'average over the rows of the array\n', np.mean(Rarray, axis = 1) # calculate the averages for all rows

print 'select all elements larger than 0.5\n', Rarray[Rarray>0.5] # select all items in array larger than 0.5



"""
Random permutation
"""
Vec = np.arange(30) # create a vector [0...29]

np.random.shuffle(Vec) # shuffle the elements of the vector (random order)

print 'shuffled Vec', Vec


"""
Section 3:

searching and sorting
	
"""

"""
Find the maximal value of a vector and its respective index/place in the vector
"""

MaxVal = np.max(Vec) #  gives the maximum value of vector

MaxInd = np.argmax(Vec) # gives the index/place of the 'maximum' value in the vector, 'arg' stands for argument


"""
Sort the shuffled vector
"""

SortVec = np.sort(Vec) # sort the vector in 'ascending' order (from low to high values)

print SortVec[:5] # prints the first 5 values of the sorted vector



"""
A bit harder:

Sort one vector on the basis of the values of another vector

"""

Rvec = np.random.rand(30) # create a vector with random values [0,1)

IndexSort = np.argsort(Rvec) # this is the tricky part. it is similar to np.argmax (see above), it returns the original positions of all elements in the newly sorted list.

# Thus the next line will give the sorted vector 'Rvec' in ascending order

print Rvec[IndexSort]

SortVec = Vec[IndexSort] # sort 'Vec' with the indexes generated when sorting 'Rvec', so this sorts 'Vec' on the basis of 'Rvec'

print SortVec[:3] # print the first 5 values of the sorted vector

