import numpy as np
import matplotlib.pyplot as plt # import the plotting library

"""
plot a line graph for a vector x against a vector y

"""
# create an empty figure number 1
fig = plt.figure(1) 

# create data
x = np.arange(10) # x-coordinates [0...9]
y = x**2 # y values = x^2 

# plot the line x = y
plt.plot(x, y)

# set title and labels for the axes
plt.title('x against x^2') 
plt.xlabel('x')
plt.ylabel('y')

# show the figure, so the user can interact with it
#~ plt.show()


"""
scatter plot of two vectors x and y

"""
# create a second figure for plotting
fig = plt.figure(2)

# create data
x = np.floor(np.random.rand(10)*20) # generate 10 random whole numbers from 0 to 20 (excluding 20)
y = np.floor(np.random.rand(10)*20) # generate 10 random whole numbers from 0 to 20 (excluding 20)

# scatter plot the x and y vectors against eachother, while adding a red color for the dots
plt.scatter(x,y, facecolor='r') 

# set title and labels for the axes
plt.title('scatter plot of x and y')
plt.xlabel('x')
plt.ylabel('y')


"""
histogram of random normal distributed data

"""
fig = plt.figure(3)
data =np.random.normal(10,1,400) # create 400 values with mu = 10 and sigma=1
plt.hist(data, bins = 30) # plot a histogram with 30 bins 
plt.title('totally random')



"""
plot the figures in the script
"""
# show the figures
plt.show()












