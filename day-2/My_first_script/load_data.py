"""
This file displayes three ways to read data from a csv file (comma-separated-values)

methods:
1 - reads the file with the csv.reader available from the csv module (Python standard library)

2 - reads the file using basic file and string manipulation available in python (more details involved)

3 - reads the file and creates a numpy array using the numpy function 'genfromtxt'.

"""

import numpy as np # import the Numpy library
import os # import the Operating System module for directory access
import csv # imports the csv module containing csv.reader

# change the working directory
#~ os.chdir(r'') 

filename = 'data.txt'



"""
First method:

load data with python csv specific IO functions

The 'csv' module makes loading 'comma-separated-values' files easy.


"""
time_axis = [] # create empty list
signal = [] # create empty list

data =csv.reader(open(filename, 'r'), delimiter = ',') # read the file with the csv reader: open() creates the file object, which is then given to the csv.reader

for line in data: # step through the lines of the file
	time_axis.append(line[0]) # append the time value to the time_axis list
	signal.append(line[1]) # append the datapoint to the signal list

#~ print signal # print the signal




"""
Second method:
load data with python with basic file / string  operators
This example is a bit harder, because it involves more steps to the same end.

"""

file = open(filename, 'r') # open the file in read mode

time_axis = [] # intialize empty lists
signal = []

for line in file: # step through the lines in the file
	line = line.strip() # remove white spaces from the line, i.e. tabs and spaces
	time, point = line.split(',') # split the line on every comma ','
	time_axis.append(time) # add the timepoint to the time_axis list
	signal.append(point) # add the datapoint to the signal list

print signal



"""
Third Method
load data with python/numpy
"""

data = np.genfromtxt(filename, delimiter=',') # generates a 2D array from the csv file

time_axis = data[:,0] # assign the first column to the time axis
signal = data[:,1] # assign the second column to the signal

print signal


