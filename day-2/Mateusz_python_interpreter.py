# basic arithmetic (usigng Python as calculator)
4+5
8 / 2 * 7
x = 12
x ** 2
y = 9 + 7 * x
print y

# logic comaprisons
4 > 0
'pindakaas' == 'sambal'
x = 7
y = 8
x <= y and x > 0
not x >= y
x is y

# lists
streets = ['Rokin', 'Spuistraat', 'Singel', 'Herengracht']
streets[0]
streets[-1]
streets[:-2]
streets[2:2] = ['Polderweg', 'Bredeweg', 'Ringdijk']
print streets
streets.append('Insulindeweg')
print streets

# tuple (immutable list)
addres = ("Will Shulman", 154000, "BSCS Stanford, 1997")
addres[0]
addres[:2]
addres[1] = 158000

# string manipulations
species = "Pinus palustris" # define a string
species[:5]
species[6:]
exclamation = "oh"
exclamation * 6
exclamation * 3 + "! That was delicious."
exclamation.capitalize()
'sad'.replace('s', 'gl')
species.find('pal')
species.count('u')
species.endswith('tris')
species.split(' ')
'123455979986'.isdigit()
'1500Euro'.isdigit()

# dictionary (assosiative array, hash map, map, key/value storage)
sphere = {'radius': 56, 'weight': 18, 'color': 'orange', 'matherial': 'copper'}
sphere['radius'] # 'radius' is a key  in the dictionary
print sphere.keys()
sphere['usage'] = ['throwing', 'bowling', 'playing with a dog']
sphere

# importing and more
import numpy
numpy.pi
import numpy as np
np.pi
from numpy import pi
pi
from numpy import fastCopyAndTranspose as fcat
fcat

# getting help
# tab completion
help numpy.ndim
dir numpy.ndim
%psearch np.dialog* # ipython magic
numpy.lookfor('convolution') # numpy looks for keyword in docstrings

