
"""
This example demonstrates some linear algebra features of numpy/scipy.
The functionality is essentially identical to the one found in MATLAB,
though with a slightly different syntax. More information on the differences
can be found at: http://www.scipy.org/NumPy_for_Matlab_Users

In this script, a polynomial regression on some noisy data points is performed.
As an excercise, you are encouraged to extend this functionality to different
types of regression. For instance, one can replace the function f by the function g,
which contains an exponential component. The polynomial regression will do
a poor job of fitting this function, but thus can be remedied by extending
the basis vector matrix B in the polyfit function.
(which then, strictly speaking, is no longer a POLYfit function)
"""

#import numpy for standary array and matrix operations
import numpy as np
#import scipy and its linear algebra subpackage
import scipy
from scipy import linalg


#define some polynomial coefficients
c = [5, 4, 3]
#define a function to do regression against
def f(c, x):
    """evaluates a polynomial given by the coefficients in c at locations x"""
    return scipy.polyval(c, x)
def g(c, x):
    """alternative function definition, including an exponential term"""
    return scipy.polyval(c[0:-1], x) + np.exp(x) * c[-1]

#define a polynomial: x=-5..5, y = f(x)
x = np.linspace(-5, +5, num = 100)
y = f(c, x)


def lstsq(B, y):
    """
    least squares approximation of the overconstrained system B * c = y
    to be used inside the polyfit function
    """
    #because B is of type matrix rather than array, multiplication means an
    #inner product rather than elementwise operation
    B = np.asmatrix(B)

    #premultiplication of the equation by B transposed corresponds to a least-squares fit
    A = B.T * B
    r = B.T * y
    #solve the (3,3) reduced linear system for c;
    c = linalg.solve(A, r)
    #alternatively, we could write:
    #c = linalg.inv(A) * r
    return c


def polyfit(x, y):
    """
    define a function to deduce the coefficients that relate the vectors x and y
    a 'least squares' fit is performed, meaning we find the coefficients that minimize
    the l2-norm of the error vector, or sum((y-B*c)**2)
    """

    #the 1d input array y is converted to an [n,1] column vector (y stacked over multiple rows)
    y = np.row_stack(y)

    #basis matrix B:
    #each column of B corresponds to a basis vector included in our fit
    B = np.matrix([x**2, x**1, x**0]).T

    #we have to solve the system B * c = y
    #solve this overconstrained system by least squares approximation
    c = lstsq(B, y)
    #we can also use the scipy built in least squares (the [0] selects the first return argument):
    #c = linalg.lstsq(B, y)[0]

    #cast [3,1] coefficient column matrix to an 1d length-3 array
    return np.asarray(c).flatten()


#noisy y vector: add some normally distributed noise to y
ny = y + np.random.normal(scale = 10, size = y.shape)

#reconstructed coefficients, found by the polyfit function defined above
rc = polyfit(x, ny)
#reconstructed y vector
ry = f(rc, x)


#Scipy already includes a polyfit function; but do we get the same results?
sc = scipy.polyfit(x, ny, deg=2)
print 'original coefficients:', c
print 'scipy result:', sc
print 'our own result:', rc
"""
The answer is yes. This of course raises the question if there is any
point to implementing our own solution, other than as an academic excercise.
The answer is yes. Our own polyfit function works only on degree two polynomials;
but we could extend it to work with polynomials of any degree, and we are also
free to mix in non-polynomial basis functions
"""

#plot the results
import matplotlib.pyplot as pp
pp.figure()
pp.plot(x, y,  color = 'green', label = 'true model')
pp.plot(x, ny, color = 'red',   label = 'measurement of model')
pp.plot(x, ry, color = 'blue',  label = 'estimation of model')
pp.xlabel('x')
pp.ylabel('y')
pp.legend()
pp.show()