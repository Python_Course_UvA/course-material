"""
This example gives an example of the pyparsing module. This module allows one
to flexibly declare arbitrary grammars for parsing text data into a useable format.

That means if you need to read in some text data from a file, in a format
that is not supported out of the box by any existing libraries,
pyparsing is probably what you are looking for.


Dependencies:
    pyparsing:
        install: type 'pip install pyparsing'
    pandas:
        windows install: download binary from http://www.lfd.uci.edu/~gohlke/pythonlibs/#pandas
        other install:  type 'pip install pandas'


This example was taken and modified from the pyparsing examples website
website:            http://pyparsing.wikispaces.com/
API documentation:  http://packages.python.org/pyparsing/
"""



#a string literal containing example data to parse
textTable = """
+-------+------+------+------+------+------+------+------+------+
|       |  A1  |  B1  |  C1  |  D1  |  A2  |  B2  |  C2  |  D2  |
+=======+======+======+======+======+======+======+======+======+
| min   |   7  |  43  |   7  |  15  |  82  |  98  |   1  |  37  |
| max   |  11  |  52  |  10  |  17  |  85  | 112  |   4  |  39  |
| ave   |   9  |  47  |   8  |  16  |  84  | 106  |   3  |  38  |
| sdev  |   1  |   3  |   1  |   1  |   1  |   3  |   1  |   1  |
+-------+------+------+------+------+------+------+------+------+
"""


def TableGrammar():
    """
    In this function, the grammar of the table above is defined.
    Pyparsing uses this grammar definition to parse any text that obeys the grammar.

    You are not advised to try and understand everything that is going on here,
    because that is quite a lot indeed. Not does this function contain a lot
    of dense information, there is much more going on 'behind the scenes'.

    Rather, it is meant as a demonstration of what can be accomplished with
    merely the knowledge of what tools are out there, a little copy-pasting,
    and a superficial understanding of what is going on;
    that is after all how this script was created in the first place.

    If you are feeling adventurous: there are some fairly obvious extensions to be made:

        Can you add rows and columns to the text data?
        Is the grammar flexible enough to handle these changes?

        The grammar currently has some fairly arbitrary rules concerning row/column names
        F.I, column names can be alphanumeric, but row names do not permit numbers
        Can you further restrict/expand the row/column name grammar?

        Currently, the grammar only accepts whole (integer) numbers as table entries.
        Can you make it accept other numeric types as well?
    """
    #import pyparsing symbols
    from pyparsing import Literal, Word, Group, ZeroOrMore, alphas, nums, delimitedList

    #define delimiters; all of them are supressed, thus never added to the parsing output
    vertDelim  = Literal('|').suppress()                                        #| is used as a vertical delimiter;
    crossDelim = Literal('+').suppress()                                        #+ is used where vertical and horizontal lines cross
    horDelim   = Word('-=').suppress()                                          #- and = are used as horizontal delimiting characters
    rowDelim   = (crossDelim + ZeroOrMore(horDelim + crossDelim)).suppress()    #row delimiters consists of vertical delimiters enclosed between cross-delimiters

    #define atomic grammar elements; these are the things we want to extract
    number     = Word(nums).setParseAction( lambda number: int(number[0]) )     #a number is a string containing only numerals; upon encountering such a string, return its integer representation
    columnName = Word(alphas + nums)                                            #column names can consist of alphanumeric characters
    rowName    = Word(alphas).setResultsName('name')                            #row names can consist of letters from the alphabet only

    #define composite grammar elements
    columnNames = delimitedList(columnName, vertDelim).setResultsName("names")  #columnNames is a list of columnName's seperated by vertical delimitors
    columnLine  = (vertDelim + vertDelim + columnNames + vertDelim).setResultsName("columns")  #the whole line containing the column definitions contains some more whitespace
    rowEntries  = delimitedList(number, vertDelim).setResultsName('data')       #the row entries are a list of numbers
    rowLine     = vertDelim + rowName + vertDelim + rowEntries + vertDelim      #a row consists of a name column followed by a list of entries
    rowsList    = ZeroOrMore(Group(rowLine)).setResultsName('rows')             #rowsList matches for a list of the 'rowLine' pattern

    #complete grammar; the rowDelims are supressed; they are not in the parsed output
    #the only thing in the output is the columnLine under the 'columns' attribute
    #and the list of rows under the 'rows' attribute
    return rowDelim + columnLine + rowDelim + rowsList + rowDelim


"""
parse the text data according to the specified grammar of the table
once we have defined our grammar, it really is that simple
"""

parsedTable = TableGrammar().parseString(textTable)

"""
in the grammer definition, we demanded the following structure of attributes for the parsing output

parsedTable.rows:              containst a list of zero or more rows
parsedTable.rows[i].name       the name attribute of each row contains a string
parsedTable.rows[i].data       the data attribute of each row contains a list of zero or more numerical values
parsedTable.columns.names      contains a list of all column names
"""

#convert the parsing output to a pandas table
import pandas
pandaTable = pandas.DataFrame(
    [row.data for row in parsedTable.rows],         #a list of lists containing all the numerical data
    [row.name for row in parsedTable.rows],         #a list of strings containing the row names
    [name for name in parsedTable.columns.names])   #a list of strings containing the column names


#print the original text data and the pandas table side-by-side
print 'Text table:'
print textTable
print 'Pandas table:'
print pandaTable
