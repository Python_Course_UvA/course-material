
"""
Animation Example

This example shows how to use the animation features of matplotlib to create
animated figures. In this case, an imageplot is animated, but the same
principles could be applied to any plot

The data being animated is the result of a Gray-Scott reaction diffusion model.
This model deals with two chemical concentrations and their interactions.
It is a profound theoretical illustration of the emergent complexity that may
result of a system containing only two simple reacting chemicals.
The parameters governing the rates of the chemical reactions can be modified.
Given the default parameter settings, a pattern emerges that is actually often
realized in nature by the notch-signalling pathway, to pattern hair follicles for instance.
Other parameters yield structures like fingerprints and leopard stripes, and
mechanisms similar to those involved in neuronal signal transduction.
Various other structures emerge which biology might never have found a use for,
but which are still striking nonetheless.

http://mrob.com/pub/comp/xmorphia/
"""


import numpy as np

#Diffusion constants for u and v. Probably best not to touch these
ru = 0.2
rv = 0.1
#Timestep size; if set too big, the simulation becomes unstable
dt = 0.8
#Number of pixels in each linear dimension
n = 100
#desired number of frames per second for the animation
fps = 30
#animation length in seconds
length = 20

"""
Various interesting parameter combinations, governing the rate of synthesis
of u and rate of breakdown of v, respectively.
Different rate parameters yield very different results, but the majority of
parameters combination do not yield interesting results at all.
So if you feel like trying something yourself, start by permuting some existing settings
"""
params = dict(
    divide_and_conquer  = (0.035, 0.099),   #dividing blobs
    aniogenesis         = (0.040, 0.099),   #a vascular-like structure
    fingerprints        = (0.032, 0.091),   #a fingerprint-like pattern
    holes               = (0.042, 0.101),
    labyrinth           = (0.045, 0.108),   #somehow this configuration does not like closed loops
    chemotaxis          = (0.051, 0.115),   #growing roots?

    #lower parameter values tend to destabilize the patterns
    unstable_blobs      = (0.024, 0.084),
    unstable_labyrinth  = (0.024, 0.079),
    unstable_holes      = (0.022, 0.072),

    #even lower parameters lead to wave-like phenomena
    swimming_medusae    = (0.011, 0.061),
    traveling_waves     = (0.019, 0.069),
    standing_waves      = (0.015, 0.055),
    trippy_chaos        = (0.025, 0.075),
)

#pick a random set of parameters
import random
key = random.choice(params.keys())
#or overwrite this random choice with one of your own, if you wish
##key = 'divide_and_conquer'
#get the rate constants from the parameter dictionary
f, g = params[key]




"""
This is where the math starts; if you dont care about it, you can skip to the next
section, which deals purely with the animation
"""

import scipy.ndimage
def diffusion(field, rate):
    """
    diffusion can be modelled as laplacian of a concentration field
    diffusion speed is proportional to the rate parameter
    boundary conditions are set to periodic (mode='wrap')
    """
    return scipy.ndimage.laplace(field, mode = 'wrap') * rate

def gray_scott_derivatives(u, v):
    """the gray-scott equation; calculate the time derivatives, given a state (u,v)"""
    reaction = u * v * v                        #reaction rate of u into v; note that the production of v is autocatalytic
    source   = f * (1 - u)                      #replenishment of u is proportional to its deviation from one
    sink     = g * v                            #decay of v is proportional to its concentration
    udt = diffusion(u, ru) - reaction + source  #time derivative of u
    vdt = diffusion(v, rv) + reaction - sink    #time derivative of v
    return udt, vdt                             #return both rates of change

def integrate(state, derivative):
    """
    forward euler integration of the equations, giveen their state and time derivative at this point
    the state after a small timestep dt is taken to be the current state plus the time derivative times dt
    this approximation to the differential equations works well as long as dt is 'sufficiently small'
    """
    for s,d in zip(state, derivative):
        s += d * dt

def simulate():
    """
    generator function to do the time integration, to be used inside the animation
    rather than computing all the image frames before starting the animation,
    the frames are computed 'on demand' by this function, returning/yielding
    the image frames one by one
    """
    #create initial concentration profiles; u=1 and v=0 everywhere
    u = np.ones(shape=(n,n), dtype = np.float32)
    v = np.zeros_like(u)
    #add some noise to a region in the middle, to kickstart the reaction
    window = v[50:70,50:70]
    window += np.random.random(size=window.shape)

    frames = fps * length
    #repeat 'frames' times
    for i in xrange(frames):

        #make 20 timesteps per frame; we dont need to show every one of them,
        #since the change from the one to the next is barely perceptible
        for r in xrange(20):
            #update the chemical concentrations
            integrate(
                state      = (u, v),
                derivative = gray_scott_derivatives(u, v))

        #every 20 iterations, yield output
        #the v field is what we yield to be plotted
        #we might as well plot u, as it visualizes the dynamics just as well
        yield v


"""
Thats the last of the math; from here on, its all about the animation
"""


#import the required matplotlib modules
import matplotlib.pyplot as pp
import matplotlib.animation as animation

#create a new figure
fig = pp.figure(key)
#create a new imageplot in the figure we just created, with some dummy zero data of the right size
img_plot = pp.imshow(np.zeros((n,n)))
#inform the imageplot that its data should be in the 0-1 range
img_plot.set_clim((0,1))


#function that is called every time the animated figure wants to update itself
def animate(data):
    #update the imageplot with the new data
    img_plot.set_data(data)
    #adjust the colormap to the minimum and maximum value found in the current data;
    #disable this line to view the absolute concentration
    img_plot.autoscale()
    return (img_plot,)   #return a tuple of all plot components that have been altered and need to be redrawn


#construct the animation
ani = animation.FuncAnimation(
    fig = fig,            #the figure that is to be animated
    func = animate,       #a function to update the figure content
    frames = simulate,    #a sequence of data, to be passed to the animation function one by one
    interval = 1000./fps, #the interval between frames in milliseconds
    blit = True)          #a technical detail

#pop up the figure and start animating
pp.show()

