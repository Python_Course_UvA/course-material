"""
Section 5:
Functions as first rate objects

Functions are objects, much as anything else in python (Although the 'object-oriented' paradigm is not totalitarian as in f.i. Ruby)

Because of this functions can i.e. be send to other functions, incorporated into lists and dictionaries and extended with instance attributes

"""

# define function 'f'
def f(x,y):
	return x+y
	
# define function 'g'	
def g(x,y):
	return x*y

# print the methods of the function object
#~ print dir(f) # uncomment please

# thus we see that the name of the function is an instance attribute of the function object. 
print 'function name = ',f.__name__


"""
Extending a function object with data attributes
"""

f.x = 2 # this is unusual, but can be handy in odd places
f.y = 5

# call a function with it's own data (weird!!!)
print 'adding data attributes to a function', f(f.x, f.y)


"""
a neat thing you can do is put functions in a list and execute them while iterating over the list

- the name of the function (i.e. 'f') without the call '()' is the reference to the function object
"""

funcList = [f,g] # put function objects f and g in a list

x,y = 3, 5 # assign values to x and y

for func in funcList: # iterate over the list of functions
	print func(x,y)


"""
Passing functions as parameters, the basis of functional programming

"""

def h(func, x,y): # this function receives a function and calls it with x and y
	
	return func(x,y)

print 'passing functions as parameters', h(f, x, y)









