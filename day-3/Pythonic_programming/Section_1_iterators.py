"""
Section 1:
Iterators:

many modern scripting language incorporate iterators in their basic functionality.

For instance in Python the 'for' loop construct is an iterator.

Example:
for num in list:
	print num

The 'for' loop iterates over the list: it converts the list to a iterator, extracts the first element of the iterator/list and removes it from the iterator.
After that it repeats this operation until the iterator/list is empty.

'can you see the iterative aspect of this repeated operation?'

The concept of iteration in this context, orginated in functional programming.

In Python iteration is handled through the iteration protocol, 
which consists of a method 'iter', which returns an iterator of the object under inspection
and the method 'next' for extracting the next value
"""

a = [10,15,3,400,5]

# dir() shows all the methods that an object (i.e. a list) has
#~ print dir(a) # see the __iter__ method printed in the list (uncomment please)

a = iter(a) # this explicit step calling __iter__ is often done implicitly

print a.next() # extracts a value from the iterator
print a.next()
print a.next()
print a.next()
print a.next()
#~ print a.next() #uncomment this line if you want to see the exception that is raised when the iterator is depleted


