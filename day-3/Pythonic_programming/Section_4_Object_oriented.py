"""
Section 4:
one of the shortest introduction to Object-oriented programming (OOP), good luck!

note: Section 4 is lighter but assumes some knowledge about what an object, instance, attribute and class are.

Study case:

Let's say your boss wants you to code the properties of a lot of people ['jan', 'simone', 'terry', 'judith']
and include some basic methods of human beings for each (eating and getting hungry)

the naive way to this is to make a data set for each person and act on it with functions (bad idea)

A better way to do it is to write a 'class' describing 'humans' and capture the facts of live in that class.

You can than fill in specific information for each individual you have to code, by making 'class instances'.

Let's create the class first.
"""
import random # python's random generator library

# here we gowowo!
class Human():
	"""
	- this class captures the basics of being human (tamagotchi style ;)
	
	- it intializes a person with a certain age, name and stomach content
	
	- it then provides two methods for every person (instance)
	
		1- introspection whether someone is hungry
		
		2- the 'eat' method for stilling the hunger (sorry no coffee method yet, you want to give that a try?)
	
	"""
	
	def __init__(self, name, age): # create a person with name, age and stomach content
		
		self.name = name
		self.age = age
		self.stomach = 'full'
		
	def hungry(self): # is the person hungry?
		
		dice = random.uniform(0,1) # random number between [0,1)

		if self.stomach == 'full': # stomach was full, but is it still full?
			if dice>0.6: # change the state of stomach based on the roll of a dice
				self.stomach = 'empty'
				print self.name+' is hungry and wants to eat'
			else: # no change of state
				print self.name+' is not hungry'
		else: # the person was already hungry
			print self.name+' is hungry and wants to eat'			
		
	def eat(self): # can you get the person to eat?	
		if self.stomach == 'empty': # person will eat
			self.stomach = 'full'
			print self.name +' has eaten the contents of the refrigerator'
		else: #person will not eat
			print self.name+" isn't hungry and refuses to eat"
			
"""
This is a class capturing the 'pattern' of being human

Now how to create the individual persons?

you have to make a class 'instance' for each person, via calling the __init__ method (not explicitely)

"""

# Let's see, their characteristics are:
names = ['jan', 'simone', 'terry', 'judith']			
ages = [50, 35, 45, 23]		

# now let's create the persons and put them in a dictionary, the dictionary just came out handy here

Persons = {} # initialize empty dict

for name, age in zip(names, ages): # iterate over the characteristics of the persons
	
	# in the next line the persons are created
	person = Human(name, age) # create class instance!!!, by calling __init__ with name and age, implicitely
	
	Persons[name] = person # put the person in a dictionary, with his/her name as 'key' of the dictionary
	
	
# now let's see whether some individuals are hungry, want to eat and how many springs they have seen
for name in ['terry', 'judith']: 	
	print 'is '+name+' hungry?'	
	Persons[name].hungry() # see  whether the person is hungry
	
	print 'Does '+name+' want to eat?'
	Persons[name].eat() # see whether the person wants to eat
	
	print 'how old is '+name+'?'
	print name+' is '+str(Persons[name].age)+' years old\n\n' # retrieve the age from the class instance (person)
	
# can you add a loop for repeated inspection of the hungry/eat questions? possible until all selected persons have eaten once?
