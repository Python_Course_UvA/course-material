"""
Section 3:
generator functions:

Generator functions behave similar to the generator expressions in section 2. 

recipe for a generator function:

1- make a block of code that calculates the values you need (any process that generates a sequence of values)

2- envelop it with a function definition 'def'

3- use the 'yield' statement to pass a calculated value (in the sequence) to the outside, one-at-a-time

"""

# 1- block of code

stringg = 'Monty Python'

for i in range(len(stringg)): 

	print stringg[i:],

print


# 2- envelop with a function

def chop(stringg):
	for i in range(len(stringg)):
		yield stringg[i:] # wow, it's a lazy and state persistant

# initialize the chop function with the string
chopper = chop(stringg) 

# print the values contained in the 'chopper' object
print 'chop string', list(chopper)

"""
Generator functions really facilitate 'code reuse' which is a good thing.

'remember the days when you hardcoded all your 'for' loops for trivial household code?'
"""

string2 = 'spamm, bacon and spamm'

chopper = chop(string2)

print 'chop string2', list(chopper)

