"""
This scripts treats some aspects of working with spike trains recorded during a electrophysiology experiment. 
A spike train is are the action potentials that are fired by a neuron in sequence.
for now we will only use the timing of the action potential, thus we have a list of spike times / spike timestamps, which we will manipulate.

"""


import numpy as np
import os
import matplotlib.pyplot as plt
import scipy.io as scio # part of scipy where io functions reside

"""
Most of you reading this have tons of matlab data lying around your lab. 
No problem, because Scipy features compatible loading and reading of any .mat file.

Let's load a spike train (spike time stamps in ms) into data
"""

filename = 'spike_train.mat' # name of matfile

chan = 12 # channel number of recording

# load the spike timestamps into memory from the MATLAB file
Stamps = scio.loadmat(filename)['ch'+str(chan)] # the reference '['ch'+str(chan)]' comes from the interface between matlab structures and python dictionaries
# the spike train are the sorted spikes from one MEA channel during a 3 hour recording 

"""
Admittedly the conversion between matlab structs and python dicts can be a bit odd, but preferences for this can be set. 
If one encapsulates the loading of MATLAB data in a function, one has to only worry about this once.

Personally I keep my timestamps as matlab files, because this is a small portable dataset for switching between matlab and python.
In a year of work I have had to switch twice for specific matlab toolboxes, that I couldn't translate at that point.

"""
"""
Part 1: 

Spike train statistics (auto-correlation and ISI)

"""
# Select a time window for the analysis
window = [4400*1000, 4900*1000] # window in ms, from 3400 s to 3900 s (in a 3 hour long experiment)

# Logical indexing for selection spike times within the window
Stamps = Stamps[(Stamps >= window[0]) & (Stamps < window[1])]

# substract starttime to reference spike times to the start of the window\
Stamps -= window[0] # -= is the inplace substraction much like i++ in C is the inplace addition (memory conservative)
"""
Let's make a binned binary signal of the spike train, this is used to calculate correlations
"""
BinSize = 20 # binsize in ms

# calculate the number of bins of size BinSize within the experimental time window
nBins = (window[1]-window[0]) / BinSize

# create an empty signal
Signal = np.zeros(nBins) # this gives you a 1D vector of zeros, neither column nor row

# add 'delta' functions to the signal for each spike
Signal[np.floor(Stamps/BinSize).astype('int')] = 1. # Index Signal with Stamps while calculating the bin number from the Stamp value

"""
Let's calculate an auto-correlation function and the inter Spike intervals

"""
AutoCor = np.correlate(Signal, Signal, mode='full') # np.correlate is much faster than scipy.signal.correlate (that's open-source for ya)

# plot the Autocorrelation as an assignment (try to add the ISI histogram as a subplot, see the pdf)

# calculate the ISI's from the time stamps
ISI = Stamps[1:]-Stamps[:-1] # the ISI's are the 2nd to 'last' spike times minus the first the (last-1) spike times

# plot the ISI as an assignment (try adding the Autocor as an subplot, see the pdf)

"""
See the assignment for exercises on plotting the auto-correlation and ISI data

"""

"""
Part 2:

- Oscillation Frequency of a neuron is defined as dominant oscillation in the Autocorrelation function

For now we will calculate the dominant oscillation frequency like this (code it yourself, refer to the .pdf).

1 - remove the middle peak from the auto-correlation function (by indexing and assigning a constant value)

2 - calculate the fourier transform of this peakless auto-correlation function (you can use scipy.fftpack.fft)

4 - plot the power spectrum (square of the modulus (or absolute value) of the fourier output)

3 - determine which frequency is most prominent in the spectrum calculated at 2

"""





