"""
Image analysis example

Takes inspiration from:
    http://pythonvision.org/basic-tutorial
    http://scipy-lectures.github.com/advanced/image_processing/index.html

The general idea: take a typical microscopy picture containing a bunch of objects,
and reliably seperate them from the background and eachother
Then isolate the elements of interest, and compute some simple statistics about them

Requires installation of scikits.image 0.4
    pip install skimage
    or for windows, download scikits.image from (http://www.lfd.uci.edu/~gohlke/pythonlibs/#scikits-image)

NOTE: this scripts plots a lot of intermediate results for illustration.
Once you understand one part, you might as well disable them;
or add more, to get a better understanding of some other substeps involved.

Suggestions for improvement: some cells are clearly out of focus; can you remove them from the output?
You can alter the 'treshold' and 'smoothing_radius' parameters defined in the script,
to tweak the behavior of the analysis. How do these parameters influence the result?

"""

import matplotlib.pyplot as plt

#import numpy
import numpy as np
#the scipy ndimage subpackage contains a host of useful functions for image analysis
from scipy import ndimage
#import scikits.image and its subpackages; more image analysis functions
import skimage as image
import skimage.io
import skimage.morphology


#read an image file
data = ndimage.imread('dna.jpeg')
#display the data we just loaded
plt.figure('Data to be analyzed')
plt.imshow(data)
plt.show()



#a treshold value to use; pixels below this value are presumed to be background
treshold = 20
#use the treshold value to create a region of interest mask
roi_mask = data > treshold
#compute the mean value of the background pixel intensity
background = np.ma.masked_array(data, roi_mask).mean()
#plot a histogram of all pixel values
plt.figure('Histogram of intensity values; treshold in red, mean background in green')
plt.hist(data.flatten(), bins = 50)
plt.axvline(treshold, linewidth=2, color='red')
plt.axvline(background, linewidth=2, color='green')
plt.show()
#the roi mask we get this way could be better...
plt.figure('naive tresholding result')
plt.imshow(roi_mask)
plt.show()
#roi mask cleanup; if the mask contains any holes, fill them up
roi_mask = ndimage.binary_fill_holes(roi_mask)
#create a disk stencil, of radius 10
disk = image.morphology.disk(10)
print 'this is what the radius 10 disk stencil looks like:'
print disk
#remove structures smaller than the disc from the roi mask (look up ndimage.binary_opening for more information)
roi_mask = ndimage.binary_opening(roi_mask, disk)
#enlarge the mask a bit, by dilating it with the disk
roi_mask = ndimage.binary_dilation(roi_mask, disk)
#the resulting roi mask is pretty much what we are looking for...
plt.figure('cleaned up region-of-interest mask')
plt.imshow(roi_mask)
plt.show()



def find_local_maxima(image):
    """
    a function to detect the local maxima of an image
    quite a common operation, but not included directly in scipy unfortunately
    you cant always be lucky; we'll write it ourselves
    given a 2d image 'image', we return a binary image of the same size,
    where True values represent a local maximum
    """

    #for each pixel, calculate its largest neighbor by use of a maximum filter
    #the stencil used is 3x3, minus the center pixel value itself
    neighbors = np.array([
        [1,1,1],
        [1,0,1],
        [1,1,1]])

    #find the maximum neighbor value of each pixel
    maximum_neighbor = ndimage.maximum_filter(image, footprint = neighbors, mode='constant', cval = 0)

    #if for any pixel, its biggest neighbor is smaller than itself, the pixel must be a local maximum
    return maximum_neighbor < image


#correct for the mean background as found above, and convert the image to float type
data = (data - background).astype(np.float32)
#label the individual cells
#find local maximum pixels in a smoothed image. the smoothing radius is an important
#parameter, as it determines what we consider noise and what we consider to be interesting objects
#too low a value, and small features may unduely be considered a individual cells
#too high a value, and adject cells may be considered to be a single cell
smoothing_radius = 20
smoothed = ndimage.gaussian_filter(data, smoothing_radius)
maxima_mask = find_local_maxima(smoothed)
maxima_mask *= roi_mask
#assign a unique integer value to each maximum identified
labels, count = ndimage.label(maxima_mask)
print count, 'maxima detected'
#perform a watershed transform. google can explain it better than a few lines of comment
#but the general idea is to propagate the labels to all pixels that lie in the 'drainage basin'
#of a label; where a drop of water would flow to if falling on the surface of the heightfield defined by 'basin'
#these are all the pixels that in some way are naturally associated with the local maximum
basin = ndimage.gaussian_filter(-data, 20)
labels = image.morphology.watershed(basin, labels, mask = roi_mask)
#each individual cell should now be marked with its own label
plt.figure('result of the watershed transform')
plt.imshow(labels)
plt.show()


#find the windows that fits around each labelled region in the image
objects = ndimage.find_objects(labels.astype(np.ubyte))
#a function to return a masked array containing cell data, given the window in which it lies
def cut(i,window):
    cell_number = i + 1
    return np.ma.masked_array(data[window], labels[window] != cell_number)
#create a list of such cell masked subimages (uses a list comprehension)
cells = [cut(i,o) for i,o in enumerate(objects)]

#compute some statistics for each cell
area = [c.count() for c in cells]       #counts all non-masked pixels
intensity = [c.mean() for c in cells]   #the mean over all non-masked pixels
#scatterplot of cell size versus total intensity
plt.figure('Scatter plot of area in pixels versus mean intensity')
plt.scatter(area, intensity)
plt.xlabel('Cell area (Pixels)')
plt.ylabel('Cell mean intensity')
plt.show()


#plot each individual cell; uncomment this section to enable it
for i,cell in enumerate(cells):
    cell_number = i + 1
    plt.figure('Cell '+str(cell_number))
    plt.imshow(cell)
    print 'Cell number: ', cell_number
    print 'Total intensity: ', cell.sum()
    plt.show()
