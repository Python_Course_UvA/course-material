"""
Sorting sequences by length
"""

from Bio import SeqIO

# read in (parse) data from fasta file
records = list(SeqIO.parse("ls_orchid.fasta", "fasta"))

# sort sequences by lenth (provide your comparison function)
records.sort(cmp=lambda x,y: cmp(len(x), len(y)))

# write results to new fasta file
SeqIO.write(records, "sorted_orchids.fasta", "fasta")


"""
plot GC content per gene
"""

from Bio import SeqIO
from Bio.SeqUtils import GC

# read fasta file, for each sequence in file get GC content and sort resulting list
gc_values = sorted(GC(rec.seq) for rec in SeqIO.parse("ls_orchid.fasta", "fasta"))

import pylab # plotting module

pylab.plot(gc_values)

# add tiltle
pylab.title("%i orchid sequences\nGC content from %0.1f to %0.1f" \
            % (len(gc_values), min(gc_values), max(gc_values)))

# add axis lables
pylab.xlabel("Genes")
pylab.ylabel("GC%")

# display the plot
pylab.show()
