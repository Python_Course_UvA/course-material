"""
This example script shows reading the file,
exrtacting data and writing them in different
format in separate files.

Iput file format:

# experiment 25.04.2003
1.5
measurements    model1  model2
0.0             0.1     1.0
0.1             0.1     0.188
0.2             0.2     0.25

Output files:
measurements.dat
model1.dat
model2.dat

format:

0   0.1
1.5 0.1
3   0.2

This type of format is used by most plotting software.
"""

#!/usr/bin/env/ python

import sys, math, string

# define imput file
input_file = './data/example.dat'

# open file for reading
infile = open(input_file, 'r')

# read first (comment) line
line = infile.readline()

# read in delta t value from second line
# data read from file are strings which need to be converted
dt = float(infile.readline())

# read in headings for columns
ynames = infile.readline().split()

# create a list of new files for output
outfiles = [] # initialise empty list
for name in ynames:
    outfiles.append(open(name + '.dat', 'w'))

# initialise t value
t = 0.0

# and read the rest of file
for line in infile:
    yvalues = line.split()
    if len(yvalues) == 0: continue # skip blank lines
    for i in range(len(outfiles)):
        outfiles[i].write('%12g %12.5e\n' % (t, float(yvalues[i])))
    t+= dt

# close output files
for file in outfiles: file.close()

