"""
This example script show how to convert .tiff images in
specified direcotry.
"""

# import libraries

# utility functions facilitating work with operating system
import os

# python imaging library, used for simple image manipulations
from PIL import Image

# name of dir with images
img_dir = 'images'
data_dir = 'data'

# construct full path
# get current working directory
cwd = os.getcwd()

# concatenate path in system intelligent way
img_path = os.path.join(cwd, data_dir, img_dir)

# get list of files in the directory
files_list = os.listdir(img_path)

# iterate over files list
for f in files_list:
    # check if it is jpg file
    if f.endswith('.tif'):
        # construct again path to the image
        full_name = os.path.join(img_path, f)

        # open image
        img = Image.open(full_name)

        # create name for new file
        # get path and name without file extension
        path_prefix = os.path.splitext(full_name)[0]

        # add new extension
        gif_name = path_prefix + '.gif'

        # check if the file does not already exist
        if os.path.exists(gif_name):
            # if it exists tell user about it
            print "File: %s already exists." % gif_name
        else:
            # in other case save as tif
            img.save(gif_name)
            # and tell user about created new file
            print "Created file " + gif_name
