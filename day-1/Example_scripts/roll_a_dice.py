import random
import sys

# no of experiments
N = 1000

# no of dice
ndice = 10

# how many dice with 6 eyes do we want
nsix = 5

# no of successful events
M = 0

# experiment
for i in range(N):
    six = 0 # init no of dice with six eyes
    for j in range(ndice):
        # Roll a dice
        r = random.randint(1,6)
        if r == 6:
            six += 1

    # did we make it?
    if six >= nsix:
        # increment no of successful events
        M += 1

# calculate probability results
p = float(M)/N
print 'probability', p


